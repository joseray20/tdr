<?php
require('conexion.php');

$listasubrespuestas = $_POST['lista'];
$idPaciente = $_POST['idPaciente'];
$subLista = $_POST['subLista'];
$subListaID = $_POST['subListaID'];

function calcularUmbral($adherencia)
{
    //var_dump(floatval($adherencia));
    if (floatval($adherencia) <= 39) {
        return "Pobre";
    } elseif (floatval($adherencia) >=40 && floatval($adherencia)<=59.99) {
        return "Menos que Regular";
    } elseif (floatval($adherencia) >=60 && floatval($adherencia)<=79.99) {
        return "Regular";
    } elseif (floatval($adherencia) >=80 && floatval($adherencia)<=94.99) {
        return "Bueno";
    } elseif (floatval($adherencia) >=95) {
        return "Excelente";
    }
}

if ($subLista == 1) {
    $sqlCalcularAdherencia_SubCriterio = 'CALL adherencia_X_subcriterios(1, 2,
                                                        '.mysqli_real_escape_string($enlace, $subListaID).'
                                                        ,'.mysqli_real_escape_string($enlace, $idPaciente).')';

    $resultado_adherencia = [];
    $result_AdherenciaSubCriterio = mysqli_query($enlace, $sqlCalcularAdherencia_SubCriterio);
    if ($result_AdherenciaSubCriterio) {
        $row1 = mysqli_fetch_array($result_AdherenciaSubCriterio, MYSQLI_ASSOC);
        $Umbral = calcularUmbral($row1["Porcentaje_Adherencia"]);
        $respuestaSubCriterio["CantidadCriteriosCumplen"] = $row1["subCriterios_Que_Cumplen"];
        $respuestaSubCriterio["CantidadCriteriosNoCumplen"] = $row1["subCriterios_No_Cumplen"];
        $respuestaSubCriterio["porcentajeAdherencia"] = $row1["Porcentaje_Adherencia"];
        $respuestaSubCriterio["umbralAdherencia"] = $Umbral;
        $resultado_adherencia["AdherenciaSubLista"] = $respuestaSubCriterio;

        mysqli_next_result($enlace);

        $sqlGuardaAdherencia_SubCriterio = 'CALL guardarAdherencia_x_SubLista('.mysqli_real_escape_string($enlace, $idPaciente).',
                                                          "'.mysqli_real_escape_string($enlace, $row1["Porcentaje_Adherencia"]).'",
                                                          "'.mysqli_real_escape_string($enlace, $Umbral).'")';
        $result_guardar = mysqli_query($enlace, $sqlGuardaAdherencia_SubCriterio);
        if ($result_guardar) {
            mysqli_free_result($result_AdherenciaSubCriterio);
        }
    }
    mysqli_next_result($enlace);
    $sqlCalcularAdherencia = 'CALL adherencia_X_criterios(1, 2,
                                                      '.mysqli_real_escape_string($enlace, $listasubrespuestas).'
                                                      ,'.mysqli_real_escape_string($enlace, $idPaciente).')';

    $result_Adherencia = mysqli_query($enlace, $sqlCalcularAdherencia);
    if ($result_Adherencia) {
        $row2 = mysqli_fetch_array($result_Adherencia, MYSQLI_ASSOC);
        $respuesta["CantidadCriteriosCumplen"] = $row2["criterios_Que_Cumplen"];
        $respuesta["CantidadCriteriosNoCumplen"] = $row2["criterios_No_Cumplen"];
        $respuesta["porcentajeAdherencia"] = $row2["Porcentaje_Adherencia"];
        $respuesta["umbralAdherencia"] = calcularUmbral($row2["Porcentaje_Adherencia"]);
        $resultado_adherencia["AdherenciaListaPadre"] = $respuesta;

        mysqli_next_result($enlace);

        $sqlGuardaAdherencia_Criterio = 'CALL guardarAdherencia_x_Lista('.mysqli_real_escape_string($enlace, $idPaciente).',
                                                          "'.mysqli_real_escape_string($enlace, $row2["Porcentaje_Adherencia"]).'",
                                                          "'.mysqli_real_escape_string($enlace, calcularUmbral($row2["Porcentaje_Adherencia"])).'")';
        $result_guardar = mysqli_query($enlace, $sqlGuardaAdherencia_Criterio);
        if ($result_guardar) {
            mysqli_free_result($result_Adherencia);
        }
    }
    echo json_encode($resultado_adherencia);
} else {
    $sqlCalcularAdherencia = 'CALL adherencia_X_criterios(1, 2,
                                                        '.mysqli_real_escape_string($enlace, $listasubrespuestas).'
                                                        ,'.mysqli_real_escape_string($enlace, $idPaciente).')';

    $result_Adherencia = mysqli_query($enlace, $sqlCalcularAdherencia);

    if ($result_Adherencia) {
        $row = mysqli_fetch_array($result_Adherencia);
        $adherencia = 1;
        if ($row["Porcentaje_Adherencia"]>= 80) {
            $adherencia = 0;
        }
        $Umbral = calcularUmbral($row["Porcentaje_Adherencia"]);
        $respuesta["CantidadCriteriosCumplen"] = $row["criterios_Que_Cumplen"];
        $respuesta["CantidadCriteriosNoCumplen"] = $row["criterios_No_Cumplen"];
        $respuesta["porcentajeAdherencia"] = $row["Porcentaje_Adherencia"];
        $respuesta["umbralAdherencia"] = $Umbral;
        $respuesta["Adherencia"] = $adherencia;
        $resultado_adherencia["AdherenciaListaPadre"] = $respuesta;

        mysqli_next_result($enlace);

        $sqlGuardaAdherencia_Criterio = 'CALL guardarAdherencia_x_Lista('.mysqli_real_escape_string($enlace, $idPaciente).',
                                                          '.mysqli_real_escape_string($enlace, $row["criterios_Que_Cumplen"]).',
                                                          '.mysqli_real_escape_string($enlace, $row["criterios_No_Cumplen"]).',
                                                          "'.mysqli_real_escape_string($enlace, $row["Porcentaje_Adherencia"]).'",
                                                          "'.mysqli_real_escape_string($enlace, $Umbral).'",
                                                          '.mysqli_real_escape_string($enlace, $adherencia).')';

        $result_guardar = mysqli_query($enlace, $sqlGuardaAdherencia_Criterio);
        if ($result_guardar) {
            mysqli_free_result($result_Adherencia);
        }
        echo json_encode($resultado_adherencia);
    }
}

mysqli_close($enlace);
