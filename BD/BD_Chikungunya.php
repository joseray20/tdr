<?php
require('conexion.php');

$idLista = $_POST['idLista'];
$columnas = [];

$sql1 = 'CALL get_pacientes('.mysqli_real_escape_string($enlace, $idLista).')';
$sql2 = 'CALL get_codeColumns('.mysqli_real_escape_string($enlace, $idLista).')';
$result2 = mysqli_query($enlace, $sql2);

$count = 0;
while ($row2 = mysqli_fetch_array($result2)) {
    $colum['idcolumna'] = $count;
    $colum['codigo'] = $row2['codigo'];
    array_push($columnas, $colum);
    $count = $count + 1;
}

mysqli_next_result($enlace);
$result = mysqli_query($enlace, $sql1);
$BD = [];

if ($result) {
    $c = mysqli_field_count($enlace) - 1;
    while ($row = mysqli_fetch_array($result)) {
        $respuesta = [];
        $b = 1;
        while ($b <= $c) {
            $count = $b;
            array_push($respuesta, $b = $row[$b]);
            $b = $count + 1;
        }
        mysqli_next_result($enlace);
        $sql3 = 'CALL get_respuestas('.mysqli_real_escape_string($enlace, $row['idpaciente']).')';
        $result3 = mysqli_query($enlace, $sql3);

        if ($result3) {
            $contador = 19;
            $respuestas = [];
            while ($row3 = mysqli_fetch_array($result3)) {
                $count = $count + 1;
                $valor = $row3['Respuesta'];
                array_push($respuestas, $contador = $valor);
            }
            $array_Final = array_merge($respuesta, $respuestas);
            mysqli_free_result($result3);
        }
        array_push($BD, $array_Final);
    }
    $response['data'] = $BD;
    $response['columns'] = $columnas;
    echo json_encode($response);
    mysqli_free_result($result);
    mysqli_free_result($result2);
    mysqli_close($enlace);
}
