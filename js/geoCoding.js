$(document).ready(function() {
    $("#request").click(function() {
        var direccion = $("#direccion").val();

        var xmin, xmax, ymin, ymax, x, y;

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            url: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?",
            type: "get", //send it through get method
            data: {
                SingleLine: direccion,
                f: "json",
                city: "La Dorada",
                region: "Caldas",
                countryCode: "COL",
                outSR: '{"wkid": 102100,"latestWkid": 3857}',
                outFields: "Match_addr,Addr_type,StAddr,City",
                maxLocations: 5
            },
            success: function(response) {
                console.log(response);
                $.each(response.candidates, function(key, value) {
                    xmin = value.extent.xmin;
                    xmax = value.extent.xmax;
                    ymin = value.extent.ymin;
                    ymax = value.extent.ymax;
                    x = value.location.x;
                    y = value.location.y;
                });
            },
            error: function(xhr) {
                alert(xhr);
            }
        });

        var dataRequest = {
            f: "json",
            geometry: "{x: " + x + ",y: " + y + ", spatialReference: {wkid: 102100,latestWkid: 3857}}",
            tolerance: 30,
            returnGeometry: false,
            mapExtent: "{xmin:" + xmin + " ,ymin: " + ymin + ",xmax: " + xmax + ",ymax: " + ymax + ",spatialReference: {wkid: 102100,latestWkid: 3857}}",
            imageDisplay: "1062,239,96",
            geometryType: "esriGeometryPoint",
            sr: 102100,
            layers: "all:29,0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,31,33,34"
        };

        $.ajax({
            async: false,
            cache: false,
            url: "http://visor.codigopostal.gov.co/arcgis/rest/services/Mapa472/MapServer//identify?",
            type: "get", //send it through get method
            data: dataRequest,
            dataType: "json",
            success: function(response) {
                console.log(response);
                var postalCode = response.results[0].attributes.Codigo_Postal;
                for (var i = 0; i < response.results.length; i++) {
                    if (response.results[i].layerName === "Barrio") {
                        var barrio = response.results[i].attributes.Nombre;
                        break;
                    }
                }
                $("#codigoPostal").text(postalCode);
                $("#barrio").text(barrio);
            },
            error: function(xhr) {
                alert(xhr);
            }
        });
    });
});
