window.addEventListener('load', fecha, false);

function fecha() {

    var select = $('#tipoLista');

    $.ajax({
        type: "POST",
        url: "BD/getLista.php",
        success: function(respuesta) {
            var listas = jQuery.parseJSON(respuesta);
            $.each(listas, function(key, value) {
                select.append('<option value=' + value.idLista + ' consecutivo=' + value.consecutivoLista + ' subLista=' + value.subLista + '>' + value.nombreLista + '</option>');
            });
        }
    });
}

function tipoLista(sel) {
    $('#loading').css("display", "");
    var idLista = sel.options[sel.selectedIndex].value;
    var $table = $('#table_BD');

    if (idLista == 0) {
        $('#loading').css("display", "none");
        $('#table_BD').bootstrapTable('destroy');
    } else {
        $('#table_BD').bootstrapTable('destroy');
        $.ajax({
            type: "POST",
            data: {
                'idLista': idLista
            },
            dataType: "json",
            url: "BD/BD_Chikungunya.php",
            success: function(respuesta) {
                var columnas = [];
                var column_id = 13;
                $.each(respuesta.columns, function(index, item) {
                    var columns = new Object();
                    columns.field = item.idcolumna;
                    columns.title = item.codigo;
                    columns.align = 'center';
                    if ((idLista == 4) && (item.idcolumna == 7 || item.idcolumna == 9)) {
                        columns.visible = false;
                    }
                    columnas.push(columns);
                });
                $('#loading').css("display", "none");
                $('#table_BD').bootstrapTable({
                    data: respuesta.data,
                    columns: columnas
                });
            }
        });
    }
}
